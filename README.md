# Nx-Kata #1

## Contexte

Parfois, lorsque 'llon utilise une API externe, toutes les informations requises ne sont pas disponibles sur un seul call (under-fetching).

Par exemple pour lister via la GraphAPI la liste des utilisateurs AVEC leur photos de profil.

Il faut donc utiliser plusieurs calls en prenant garde des conséquences que cela peut avoir sur les performances.

Ici, pour simplifier la tâche un autre example sera utilisé, afin d'utiliser un API publique et ne pas avoir à gérer l'authentification. 

## Objectif

Créer une page qui liste les open issues d'un repository github grâce à l'API.

Prendre par example le [repo de typescript](https://github.com/microsoft/TypeScript) qui a 4.4k open issues (ou n'importe quel repo de votre choix, pourvu qu'il y ait beaucoup d'open issues).

Le but est de lister les issues par *ordre antéchronologique de création*.

La liste devra présenter le titre, la personne qui a report l'issue et les trois derniers repositories créés par l'auteur du ticket de bug (par ordre antéchronologique de création également).

Prévoir une pagination et un boutton "Load more" pour fetch plus de résultats.

Vous êtes libres d'utiliser n'importe quel techno (Angular, Vue, React, Svelte, jQuery, Vanillia JS :O).

Example de rendu:

[![See expected result](https://img.youtube.com/vi/wLed04-cX5s/0.jpg)](https://www.youtube.com/watch?v=wLed04-cX5s)

## Temps

1h30

## Bonus

Si vous terminez en avance, n'hésitez pas à faire des améliorations telles que:

* Donner le nombre total de résultats (ou une estimation)
* Créer un petit form avec deux champs "owner" / "repo" pour changer de repo
* Mettre en place de l'infinite scroll
* Aggréger également la date du dernier commentaire
* Mettre un loader le temps que les résultats arrivent
* ...

## Ressources utiles

* API Github ([REST](https://docs.github.com/en/rest) / [GraphQL](https://docs.github.com/en/graphql))
